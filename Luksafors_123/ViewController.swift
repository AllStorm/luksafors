//
//  ViewController.swift
//  Luksafors_123
//
//  Created by Mark on 15/02/2018.
//  Copyright © 2018 Kaspars_Martins. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var DatePicker: UIDatePicker!
    
    @IBOutlet weak var TekstsLabel: UILabel!
    @IBOutlet weak var Aplis: UIView!
    
    let Balts = [UIColor.white.cgColor, UIColor.white.cgColor]
    let Sarkans = [UIColor.red.cgColor, UIColor(hue: 0, saturation: 1, brightness: 0.65, alpha: 1.0).cgColor]
    let Dzeltens = [UIColor.yellow.cgColor, UIColor(hue: 0.1667, saturation: 1, brightness: 0.66, alpha: 1.0).cgColor]
    let Zals = [UIColor.green.cgColor, UIColor(hue: 0.3333, saturation: 1, brightness: 0.58, alpha: 1.0).cgColor]
    let gradient = CAGradientLayer()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: Aplis.frame.size.width, height: Aplis.frame.size.width))
        gradient.frame = view.bounds
        gradient.colors = Sarkans
        Aplis.layer.insertSublayer(gradient, at: 0)
        
        //Aplis.backgroundColor = #colorLiteral(red: 0, green: 1, blue: 0, alpha: 1) /* #00ff00 */
        Aplis.layer.cornerRadius = Aplis.frame.size.width/2
        Aplis.clipsToBounds = true
    }
    @IBAction func TimeAction(_ sender: Any, forEvent event: UIEvent) {
        DatePicker.datePickerMode = .time
        let date = DatePicker.date
        let components = Calendar.current.dateComponents([.hour, .minute], from: date)
        let hour = components.hour!
        let minute = components.minute!
        let counter = ((hour - 6) * 60 + minute) % 7
        if (hour < 6) {
            gradient.colors = Balts
        }
        else{
            switch counter{
            case 1, 2:
                gradient.colors = Zals
            case 4,5,6:
                gradient.colors = Sarkans
            default:
                gradient.colors = Dzeltens
            }
            Aplis.layer.insertSublayer(gradient, at: 0)
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

